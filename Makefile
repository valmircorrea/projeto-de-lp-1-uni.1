LIB_DIR = ./lib
INC_DIR = ./include
SRC_DIR = ./src
OBJ_DIR = ./build
BIN_DIR = ./bin
DOC_DIR = ./doc
TEST_DIR = ./test
IMG_DIR = ./images
DATA_INPUT_DIR = ./data/input
DATA_OUTPUT_DIR = ./data/output

CC = g++
CPPLAGS = -Wall -pedantic -ansi -std=c++14 -I. -I$(INC_DIR) -g -O0

RM = rm -rf
RM_TUDO = rm -fr

PROG = multimat

.PHONY: all clean debug doc doxygen gnuplot init valgrind

all: init $(PROG)

debug: CFLAGS += -g -O0
debug: $(PROG)

init:
	@mkdir -p $(BIN_DIR)/
	@mkdir -p $(OBJ_DIR)/
	@mkdir -p $(DATA_OUTPUT_DIR)/
	@mkdir -p $(IMG_DIR)/

$(PROG): $(OBJ_DIR)/main.o $(OBJ_DIR)/calcs_estatisticos.o $(OBJ_DIR)/funcoes_arqs.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPLAGS) -o $(BIN_DIR)/$@ $^
	@echo "*** [Executavel $(PROG) criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/main.o: $(SRC_DIR)/main.cpp $(INC_DIR)/funcoes_arqs.h $(INC_DIR)/funcoes_matriz.h $(INC_DIR)/produto_matriz.h $(INC_DIR)/calc_tempo.h
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/calcs_estatisticos.o: $(SRC_DIR)/calcs_estatisticos.cpp $(INC_DIR)/calcs_estatisticos.h
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/funcoes_arqs.o: $(SRC_DIR)/funcoes_arqs.cpp $(INC_DIR)/funcoes_arqs.h $(INC_DIR)/calcs_estatisticos.h
	$(CC) -c $(CPPLAGS) -o $@ $<

# Alvo para a execução do Valgrind:
valgrind:
	valgrind --leak-check=full --show-reachable=yes -v ./bin/multimat 2 4 8 16 32

doxygen:
	doxygen -g

doc:
	@echo "====================================================="
	@echo "Limpando pasta $(DOC_DIR)"
	@echo "====================================================="
	$(RM_TUDO) $(DOC_DIR)/*
	@echo "====================================================="
	@echo "Gerando nova documentação na pasta $(DOC_DIR)"
	@echo "====================================================="
	doxygen Doxyfile

gnuplot:
	@echo "====================================================="
	@echo "Gerando gráfico 'Tempo médio de execução dos algoritmos'"
	@echo "====================================================="
	gnuplot -e "filename='stats-ite.dat'" ./script/plot.gnuplot
	@echo "====================================================="
	@echo "*** [Tempo_Medio.png gerado na pasta $(IMG_DIR)] ***"
	@echo "====================================================="
	@echo 

clean:
	@echo "====================================================="
	@echo "Limpando pasta $(BIN_DIR), $(OBJ_DIR) e $(IMG_DIR)"
	@echo "====================================================="
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*
	$(RM) $(IMG_DIR)/*
	$(RM) $(DATA_OUTPUT_DIR)/*
