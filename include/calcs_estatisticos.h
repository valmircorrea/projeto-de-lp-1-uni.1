/**
* @file	    calcs_estatisticos.h
* @brief	Arquivo com as assinaturas das funções de calculos estatisticos. 
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @author   Pedro Emerick (p.emerick@live.com)
* @since	27/04/2017
* @date	    01/05/2017
*/

#ifndef CALCS_ESTATISTICOS_H_
#define CALCS_ESTATISTICOS_H_

/**
* @brief Função que calcula o maior valor de tempo de execução.
* @param *vetor Ponteiro que recebe o endereço do vetor com os valores do tempo de execução do algoritimo.
* @return Retorna o valor maximo do tempo de execução.
*/
double maximo (double *vetor);

/**
* @brief Função que calcula o menor valor de tempo de execução.
* @param *vetor Ponteiro que recebe o endereço do vetor com os valores do tempo de execução do algoritimo.
* @return Retorna o menor valor de tempo de execução.
*/
double minimo (double *vetor);

/**
* @brief Função que calcula a media dos valores do tempo de execução.
* @param *vetor Ponteiro que recebe o endereço do vetor com os valores do tempo de execução do algoritimo.
* @return Retorna a media dos valores do tempo de execução.
*/
double media (double *vetor);

/**
* @brief Função que calcula o desvio padrão dos valores do tempo de execução.
* @param *vetor Ponteiro que recebe o endereço do vetor com os valores do tempo de execução do algoritimo.
* @param media Variável que recebe a media dos valores do tempo de execução.
* @return Retorna o desvio padrão dos valores do tempo de execução.
*/
double desvio_padrao (double *vetor, double media);

#endif