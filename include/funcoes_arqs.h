/**
 * @file	funcoes_arqs.h
 * @brief	Implementação e declaração de protótipo de funções que manipulam/criam arquivos de entrada/saída
 * @author   Valmir Correa (valmircorrea96@outlook.com)
 * @author   Pedro Emerick (p.emerick@live.com)
 * @since	25/04/2017
 * @date	01/05/2017
 */

#ifndef FUNCOES_ARQS_H
#define FUNCOES_ARQS_H

#include <iostream>
using std::cerr;
using std::endl;

#include <fstream>
using std::ifstream;
using std::ofstream;
using std::fstream;

#include <string>
using std::string;

#include <cstdlib>

#include <iomanip>
using std::setw;
using std::setprecision;
using std::fixed;

#include <ios>
using std::ios;

#include "calcs_estatisticos.h"

/** 
 * @brief	Função genérica que carrega uma matriz a partir de um arquivo
 * @param 	matrizX Matriz que irá receber os valores carregados do arquivo
 * @param	n Dimensão da matriz
 */
template < typename T >
void carrega_arq (string nome_arq, T **matrizX, int n)
{
    ifstream arq (nome_arq);

    if (!arq)
    {
        cerr << "O arquivo nao foi aberto !!!" << endl;

        exit (1);
    }

    string s_temp;
    getline (arq, s_temp);

    for (int uu = 0; uu < n; uu++) {
        for (int vv = 0; vv < n; vv++) {
            if (vv != n-1)
            {
                getline (arq, s_temp, ' ');
                matrizX [uu][vv] = atoi (s_temp.c_str ());
            }
            else 
            {
                getline (arq, s_temp);
                matrizX [uu][vv] = atoi (s_temp.c_str ());
            }
        }
    }

    arq.close ();
}

/** 
 * @brief	Função genérica que cria um arquivo de saída com a dimensão e os valores de uma matriz
 * @param 	matrizC Matriz para colocar os seus valores no arquivo
 * @param	n Dimensão da matriz
 */
template < typename T >
void cria_arq (string nome_arq, T **matrizC, int n)
{
    ofstream arq (nome_arq);

    if (!arq)
    {
        cerr << "O arquivo nao foi aberto !!!" << endl;

        exit (1);
    }

    arq << n << " " << n << endl;
    for (int ii = 0; ii < n; ii++) {
        for (int jj = 0; jj < n; jj++)
        {
            arq << matrizC [ii][jj] << " "; 
        }

        arq << endl;
    }

    arq.close ();
}

/**
* @brief Função que escreve o tempo de execução dos algoritmos em arquivos.
* @param ii Variavel que recebe a posição em que esta no vetor de parametros.
* @param *tempo_I Ponteiro que recebe um vetor com os tempos de execução do algoritmo iterativo.
* @param *tempo_R Ponteiro que recebe um vetor com os tempos de execução do algoritmo recursivo.
* @param n Variavel qe recebe a dimensao da matriz.
*/
void saida_experimentacao (int ii, double *tempo_I, double *tempo_R, int n);

#endif