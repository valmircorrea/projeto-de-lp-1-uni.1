/**
 * @file	funcoes_matriz.h
 * @brief	Implementação das funções que fazem operações com matrizes
 * @author	Pedro Emerick (p.emerick@live.com)
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	25/04/2017
 * @date	28/04/2017
 */

#ifndef FUNCOES_MATRIZ_H
#define FUNCOES_MATRIZ_H

/** 
 * @brief	Função genérica que libera uma matriz alocada dinamicamente
 * @param 	xx Matriz usada
 * @param	n Dimensão da matriz
 */
template < typename T >
void delete_matriz (T **xx, int n)
{
    for(int ii = 0; ii < n; ii++)
        delete [] xx[ii];

    delete [] xx;
}

/** 
 * @brief	Função genérica que particiona uma determinada matriz em 4 partes
 * @param 	MatrizX1 Matriz usada para colocar os valores de uma parte do particionamento das matrizes
 * @param 	MatrizX2 Matriz usada para colocar os valores de uma parte do particionamento das matrizes
 * @param 	MatrizX3 Matriz usada para colocar os valores de uma parte do particionamento das matrizes
 * @param 	MatrizX4 Matriz usada para colocar os valores de uma parte do particionamento das matrizes
 * @param 	MatrizY Matriz que será particionada
 * @param	n Dimensão das matrizes de cada partição
 */
template < typename T >
void particiona_matriz (T** matrizX1, T** matrizX2, T** matrizX3, T** matrizX4, T** matrizY, int n)
{
    for (int ii = 0; ii < n/2; ii++) {
        for (int jj = 0; jj < n/2; jj++) {
            matrizX1 [ii][jj] = matrizY [ii][jj];
        }
    }

    int kk = n/2;

    for (int ii = 0; ii < n/2; ii++) {
        for (int jj = 0; jj < n/2; jj++) {
            matrizX2 [ii][jj] = matrizY [ii][kk];
            
            kk++;
        }
        kk = n/2;
    }

    int ll = n/2;

    for (int ii = 0; ii < n/2; ii++) {
        for (int jj = 0; jj < n/2; jj++) {
            matrizX3 [ii][jj] = matrizY [ll][jj];
        }

        ll++;
    }

    kk = n/2;
    ll = n/2;

    for (int ii = 0; ii < n/2; ii++) {
        for (int jj = 0; jj < n/2; jj++) {
            matrizX4 [ii][jj] = matrizY [ll][kk];
            
            kk++;
        }
        kk = n/2;
        ll++;
    }

}

/** 
 * @brief	Função genérica que soma duas matrizes e coloca o resultado em outra matriz
 * @param 	MatrizX Matriz que recebe os valores resultantes da soma
 * @param 	MatrizY Matriz usada para soma
 * @param 	MatrizZ Matriz usada para soma
 * @param	n Dimensão das matrizes de cada partição
 */
template < typename T >
void soma_matrizes (T **matrizX, T **matrizY, T **matrizZ, int n)
{
    for (int ii = 0; ii < n; ii++) {
        for (int jj = 0; jj < n; jj++) {
            matrizX [ii][jj] = matrizY [ii][jj] + matrizZ [ii][jj];
        }
    }
}

#endif