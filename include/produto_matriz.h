/**
* @file	    produto_matriz.h
* @brief	Arquivo com as funções para multiplicação de matrizes na forma iterativa e recursiva 
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @author   Pedro Emerick (p.emerick@live.com)
* @since	26/04/2017
* @date	    01/05/2017
*/

#ifndef PRODUTO_MATRIZ_H_
#define PRODUTO_MATRIZ_H_

#include "funcoes_matriz.h"

/**
* @brief Função que realiza o produto entre duas matrizes quadradas.
* @param **A Ponteiro de matriz que recebe o endereço de uma matriz.
* @param **B Ponteiro de matriz que recebe o endereço de uma matriz.
* @return Retorna uma matriz alocada dinâmicamente, com o resultado do produto entre duas matrizes A e B.
*/
template <typename T>
T** multiplicaI (T **A, T **B, int n) {

    T **matrizC = new T*[n];
    for(int ii = 0; ii < n; ii++)
        matrizC[ii] = new T[n];

    for (int ii = 0; ii < n; ii++) {

        for (int jj = 0; jj < n; jj++) {
            
            T soma = 0;

            for (int kk = 0; kk < n; kk++) {
                soma += ( A[ii][kk] * B[kk][jj] );  
            }   

            matrizC[ii][jj] = soma;
        }
    }

    return matrizC;
}

/** 
 * @brief	Função que realiza o produto entre duas matrizes quadradas
 * @param 	matrizA Matriz usada para multiplicação
 * @param 	matrizB Matriz usada para multiplicação
 * @param 	matrizc Matriz que irá receber o produto de duas matrizes
 * @param	n Dimensão das matrizes
 */
template < typename T >
void mult_recursiva (T **matrizA, T **matrizB, T **matrizC, int n)
{   
    if (n == 1) 
    {
        matrizC [0][0] = matrizA [0][0] * matrizB [0][0];
        
        return;
    }
    else
    {
        T **a11 = new T*[n/2];
        for(int ii = 0; ii < n/2; ii++)
            a11 [ii] = new T[n/2];

        T **a12 = new T*[n/2];
        for(int ii = 0; ii < n/2; ii++)
            a12[ii] = new T[n/2];
        
        T **a21 = new T*[n/2];
        for(int ii = 0; ii < n/2; ii++)
            a21[ii] = new T[n/2];
        
        T **a22 = new T*[n/2];
        for(int ii = 0; ii < n/2; ii++)
            a22[ii] = new T[n/2];

        particiona_matriz (a11, a12, a21, a22, matrizA, n);

        T **b11 = new T*[n/2];
        for(int ii = 0; ii < n/2; ii++)
            b11[ii] = new T[n/2];

        T **b12 = new T*[n/2];
        for(int ii = 0; ii < n/2; ii++)
            b12[ii] = new T[n/2];

        T **b21 = new T*[n/2];
        for(int ii = 0; ii < n/2; ii++)
            b21[ii] = new T[n/2];

        T **b22 = new T*[n/2];
        for(int ii = 0; ii < n/2; ii++)
            b22[ii] = new T[n/2];

        particiona_matriz (b11, b12, b21, b22, matrizB, n);

        T **c11 = new T*[n/2];
        for(int ii = 0; ii < n/2; ii++)
            c11[ii] = new T[n/2];

        T **c12 = new T*[n/2];
        for(int ii = 0; ii < n/2; ii++)
            c12[ii] = new T[n/2];

        T **c21 = new T*[n/2];
        for(int ii = 0; ii < n/2; ii++)
            c21[ii] = new T[n/2];

        T **c22 = new T*[n/2];
        for(int ii = 0; ii < n/2; ii++)
            c22[ii] = new T[n/2];

        particiona_matriz (c11, c12, c21, c22, matrizC, n);

        T **p1 = new T*[n/2];
        for(int ii = 0; ii < n/2; ii++)
            p1[ii] = new T[n/2];
        
        T **p2 = new T*[n/2];
        for(int ii = 0; ii < n/2; ii++)
            p2[ii] = new T[n/2];
        
        T **p3 = new T*[n/2];
        for(int ii = 0; ii < n/2; ii++)
            p3[ii] = new T[n/2];
        
        T **p4 = new T*[n/2];
        for(int ii = 0; ii < n/2; ii++)
            p4[ii] = new T[n/2];
        
        T **p5 = new T*[n/2];
        for(int ii = 0; ii < n/2; ii++)
            p5[ii] = new T[n/2];
        
        T **p6 = new T*[n/2];
        for(int ii = 0; ii < n/2; ii++)
            p6[ii] = new T[n/2];
        
        T **p7 = new T*[n/2];
        for(int ii = 0; ii < n/2; ii++)
            p7[ii] = new T[n/2];
        
        T **p8 = new T*[n/2];
        for(int ii = 0; ii < n/2; ii++)
            p8[ii] = new T[n/2];
        
        mult_recursiva (a11, b11, p1, n/2);
        mult_recursiva (a12, b21, p2, n/2);
        mult_recursiva (a11, b12, p3, n/2);
        mult_recursiva (a12, b22, p4, n/2);
        mult_recursiva (a21, b11, p5, n/2);
        mult_recursiva (a22, b21, p6, n/2);
        mult_recursiva (a21, b12, p7, n/2);
        mult_recursiva (a22, b22, p8, n/2);

        soma_matrizes (c11, p1, p2, n/2);
        soma_matrizes (c12, p3, p4, n/2);
        soma_matrizes (c21, p5, p6, n/2);
        soma_matrizes (c22, p7, p8, n/2);

        /** Agrupando as partições em uma matriz */
        int kk;
        int ll;

        for (int ii = 0; ii < n/2; ii++) {
            for (int jj = 0; jj < n/2; jj++) {
                matrizC[ii][jj] = c11 [ii][jj];
            }
        }

        kk = n/2;

        for (int ii = 0; ii < n/2; ii++) {
            for (int jj = 0; jj < n/2; jj++) {
                matrizC[ii][kk] = c12 [ii][jj];
                kk++;
            }
            kk = n/2;
        }

        ll = n/2;

        for (int ii = 0; ii < n/2; ii++) {
            for (int jj = 0; jj < n/2; jj++) {
                matrizC[ll][jj] = c21 [ii][jj];
            }

            ll++;
        }

        kk = n/2;
        ll = n/2;

        for (int ii = 0; ii < n/2; ii++) {
            for (int jj = 0; jj < n/2; jj++) {
                matrizC[ll][kk] = c22 [ii][jj];
                
                kk++;
            }
            kk = n/2;
            ll++;
        }

        delete_matriz (a11, n/2);
        delete_matriz (a12, n/2);
        delete_matriz (a21, n/2);
        delete_matriz (a22, n/2);

        delete_matriz (b11, n/2);
        delete_matriz (b12, n/2);
        delete_matriz (b21, n/2);
        delete_matriz (b22, n/2);

        delete_matriz (c11, n/2);
        delete_matriz (c12, n/2);
        delete_matriz (c21, n/2);
        delete_matriz (c22, n/2);

        delete_matriz (p1, n/2);
        delete_matriz (p2, n/2);
        delete_matriz (p3, n/2);
        delete_matriz (p4, n/2);
        delete_matriz (p5, n/2);
        delete_matriz (p6, n/2);
        delete_matriz (p7, n/2);
        delete_matriz (p8, n/2);
    }
}

/** 
 * @brief	Função genérica que gera uma matriz produto da multiplicação de duas matrizes
 * @param 	matrizA Matriz usada para multiplicação
 * @param 	matrizB Matriz usada para multiplicação
 * @param	n Dimensão das matrizes
 * @return Matriz produto da multiplicação
 */
template < typename T >
T** multiplicaR (T **matrizA, T **matrizB, int n)
{
    T **matrizC = new T*[n];
    for(int ii = 0; ii < n; ii++)
        matrizC[ii] = new T[n];

    mult_recursiva (matrizA, matrizB, matrizC, n);

    return matrizC;
}

#endif