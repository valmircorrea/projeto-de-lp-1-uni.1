clear
reset
set key off

set terminal png size 1024, 768 enhanced font 'Helvetica, 12'
set output './images/Tempo_Medio.png'

set xlabel 'Dimensão da Matriz'
set ylabel 'Tempo'
set key box right top inside

set title 'Tempo médio de execução dos algoritmos'

set xrange [2:1024]

plot './data/output/stats-ite.dat' using 1:4 with lines linewidth 4 title 'Forma Iterativa', './data/output/stats-rec.dat' using 1:4 with lines linewidth 4 title 'Forma Recursiva'

