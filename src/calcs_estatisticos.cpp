/**
* @file	    calcs_estatisticos.cpp
* @brief	Arquivo com as funções para o calculos estatisticos. 
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @author   Pedro Emerick (p.emerick@live.com)
* @since	27/04/2017
* @date	    01/05/2017
*/

#include <cmath>

#include "calcs_estatisticos.h"

/**
* @brief Função que calcula o maior valor de tempo de execução.
* @param *vetor Ponteiro que recebe o endereço do vetor com os valores do tempo de execução do algoritimo.
* @return Retorna o valor maximo do tempo de execução.
*/
double maximo (double *vetor) {

    double maior = vetor[0];

    for (int ii = 1; ii < 20; ii++) {

        if (maior < vetor [ii]) {

            maior = vetor[ii];
        }
    }

    return maior;
}

/**
* @brief Função que calcula o menor valor de tempo de execução.
* @param *vetor Ponteiro que recebe o endereço do vetor com os valores do tempo de execução do algoritimo.
* @return Retorna o menor valor de tempo de execução.
*/
double minimo (double *vetor) {

    double menor = vetor[0];

    for (int ii = 1; ii < 20; ii++) {

        if (menor > vetor [ii]) {

            menor = vetor[ii];
        }
    }

    return menor;
}

/**
* @brief Função que calcula a media dos valores do tempo de execução.
* @param *vetor Ponteiro que recebe o endereço do vetor com os valores do tempo de execução do algoritimo.
* @return Retorna a media dos valores do tempo de execução.
*/
double media (double *vetor) {

    double media = vetor[0];

    for (int ii = 1; ii < 20; ii++) {
        media += vetor[ii]; 
    }

    return (media / 20);
}

/**
* @brief Função que calcula o desvio padrão dos valores do tempo de execução.
* @param *vetor Ponteiro que recebe o endereço do vetor com os valores do tempo de execução do algoritimo.
* @param media Variável que recebe a media dos valores do tempo de execução.
* @return Retorna o desvio padrão dos valores do tempo de execução.
*/
double desvio_padrao (double *vetor, double media) {

    double desvio_padrao;

    double somatorio = 0;

    double temp = (double) 1 / (double) 20;
    
    for (int ii = 0; ii < 20; ii++)
    {
        somatorio += pow ((vetor[ii] - media), 2);
    }

    desvio_padrao = (double) sqrt (temp * somatorio);

    return desvio_padrao;
}