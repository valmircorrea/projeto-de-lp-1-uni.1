/**
* @file	    funcoes_arqs.cpp
* @brief	Implementação da função que gera arquivo de saída
* @author   Pedro Emerick (p.emerick@live.com)
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	26/04/1017
* @date	    01/05/1017
*/

#include "funcoes_arqs.h"

/**
* @brief Função que escreve o tempo de execução dos algoritmos em arquivos.
* @param ii Variavel que recebe a posição em que esta no vetor de parametros.
* @param *tempo_I Ponteiro que recebe um vetor com os tempos de execução do algoritmo iterativo.
* @param *tempo_R Ponteiro que recebe um vetor com os tempos de execução do algoritmo recursivo.
* @param n Variavel que recebe a dimensao da matriz.
*/
void saida_experimentacao (int ii, double *tempo_I, double *tempo_R, int n) {

    if (ii == 1) {

        ofstream stats_I ("./data/output/stats-ite.dat");
        stats_I << "Dimensao" << setw(20) << "Maximo" << setw(20) << "Minimo" << setw(20) << "Médio" << setw(25) << "Desvio Padrão" << endl;
        
        stats_I << setprecision (7) << fixed;
        stats_I << n << setw(30) << maximo (tempo_I) << setw(20) << minimo (tempo_I) << setw(20) << media (tempo_I);
        stats_I << setw(20) << desvio_padrao (tempo_I, media (tempo_I)) << endl;
        stats_I.close ();

        ofstream stats_R ("./data/output/stats-rec.dat");
        stats_R << "Dimensao" << setw(20) << "Maximo" << setw(20) << "Minimo" << setw(20) << "Médio" << setw(25) << "Desvio Padrão" << endl;
        
        stats_R << setprecision (7) << fixed;
        stats_R << n << setw(30) << maximo (tempo_R) << setw(20) << minimo (tempo_R) << setw(20) << media (tempo_R);
        stats_R << setw(20) << desvio_padrao (tempo_R, media (tempo_R)) << endl;
        stats_R.close ();
    }

    else {

        fstream stats_I ("./data/output/stats-ite.dat", ios::in | ios::out);
        
        stats_I.clear ();
        stats_I.seekg (0, stats_I.end);
        
        stats_I << setprecision (7) << fixed;
        stats_I << n << setw(30) << maximo (tempo_I) << setw(20) << minimo (tempo_I) << setw(20) << media (tempo_I);
        stats_I << setw(20) << desvio_padrao (tempo_I, media (tempo_I)) << endl;
        stats_I.close ();

        fstream stats_R ("./data/output/stats-rec.dat", ios::in | ios::out);
        
        stats_R.clear ();
        stats_R.seekg (0, stats_R.end);

        stats_R << setprecision (7) << fixed;
        stats_R << n << setw(30) << maximo (tempo_R) << setw(20) << minimo (tempo_R) << setw(20) << media (tempo_R);
        stats_R << setw(20) << desvio_padrao (tempo_R, media (tempo_R)) << endl;
        stats_R.close ();
    }
}