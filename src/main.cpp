/**
* @file	    main.cpp
* @brief	Arquivo com a função principal do programa.
* @author   Pedro Emerick (p.emerick@live.com)
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	26/04/1017
* @date	    01/05/1017
*/

#include <iostream>
using std::cerr;
using std::endl;

#include <string>
using std::string;

#include <cstdlib>

#include "funcoes_arqs.h"
#include "funcoes_matriz.h"
#include "produto_matriz.h"
#include "calc_tempo.h"

/**
* @brief Função principal que realiza as chamadas das funções para a manipulação das matrizes e seus resultados.
* @param argc Variavel que contem a quantidade de argumentos passados via do terminal.
* @param *argv[] Vetor que contem os argumentos passados via do terminal.
*/
int main (int argc, char* argv[]) {

    /** Verificar se os argumentos passados pelo usuário são válidos */
    if (argc != 10) {
        cerr << "Número de dimensões inválido! Para que o programa funcione corretamente, "; 
        cerr << "são necessarias 10 dimensões. Execute-o novamente!" << endl;

        exit(1);
    }
    else {
        for (int ii = 1; ii < argc; ii++) {
            if (atoi (argv[ii]) % 2 != 0) {
                cerr << argv[ii] << " Não é uma potencia de 2! Verifique os valores e execute programa novamente!" << endl;
                exit(1);
            }     
        }
    }
    
    for (int ii = 1; ii < argc; ii++) {

        int n = atoi (argv[ii]);

        string nome_arq_A = "./data/input/A";
        nome_arq_A += argv[ii];
        nome_arq_A += "x";
        nome_arq_A += argv[ii];
        nome_arq_A += ".txt";
        
        int **matrizA = new int* [n];
        for(int ii = 0; ii < n; ii++)
            matrizA[ii] = new int [n];

        carrega_arq (nome_arq_A, matrizA, n);

        string nome_arq_B = "./data/input/B";
        nome_arq_B += argv[ii];
        nome_arq_B += "x";
        nome_arq_B += argv[ii];
        nome_arq_B += ".txt";

        int **matrizB = new int* [n];
        for(int ii = 0; ii < n; ii++)
            matrizB[ii] = new int [n];

        carrega_arq (nome_arq_B, matrizB, n);

        double tempo_R [20];
        calc_tempo_R (tempo_R, matrizA, matrizB, n);

        double tempo_I [20];
        int **matrizC = calc_tempo_I (tempo_I, matrizA, matrizB, n);

        string nome_arq_C = "./data/output/C";
        nome_arq_C += argv[ii];
        nome_arq_C += "x";
        nome_arq_C += argv[ii];
        nome_arq_C += ".txt";

        cria_arq (nome_arq_C, matrizC, n);
        cerr << "... Arquivo " << nome_arq_C << " gerado" << endl;
        
        saida_experimentacao (ii, tempo_I, tempo_R, n);

        delete_matriz (matrizA, n);
        delete_matriz (matrizB, n);
        delete_matriz (matrizC, n);
    }

    return 0;
}